#!/bin/bash -ex
apt install -y \
  mariadb-server \
  unzip \
  apache2-utils
git clone --depth=1 https://github.com/tfutils/tfenv.git /opt/.tfenv
chmod a+rwx -R /opt/.tfenv
ln -s /opt/.tfenv/bin/* /usr/local/bin/
cat > /usr/local/bin/docker-compose <<EOF
#!/bin/bash
docker compose \$@
EOF
chmod a+x /usr/local/bin/docker-compose

ufw allow from 172.18.0.0/16 to 172.17.0.1 port 3306
ufw reload
cat > /etc/mysql/mariadb.conf.d/50-server.cnf <<EOF
#
# These groups are read by MariaDB server.
# Use it for options that only the server (but not clients) should see

# this is read by the standalone daemon and embedded servers
[server]

# this is only for the mysqld standalone daemon
[mysqld]

#
# * Basic Settings
#

#user                    = mysql
pid-file                = /run/mysqld/mysqld.pid
basedir                 = /usr
#datadir                 = /var/lib/mysql
#tmpdir                  = /tmp

# Broken reverse DNS slows down connections considerably and name resolve is
# safe to skip if there are no "host by domain name" access grants
skip-name-resolve

# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.

bind-address = 0.0.0.0

skip-external-locking

# Connection Limits
max_connections = 150                      # Reasonable number for small-medium web traffic

# Thread and Cache Settings
thread_cache_size = 16                     # Moderate number for connection reuse
table_open_cache = 400                     # Reduces memory usage while allowing cache for open tables

# InnoDB Settings (For SSD Storage)
innodb_buffer_pool_size = 3G               # Allocate 1 GB for InnoDB buffer pool (safe for 4 GB RAM setup)
innodb_log_file_size = 768M                 # Lower to 64M for moderate write loads
innodb_log_buffer_size = 128M               # Moderate size for log buffer
innodb_flush_method = O_DIRECT             # Optimize for SSD by avoiding double buffering
innodb_flush_log_at_trx_commit = 2         # Reduced durability but better performance, safe for e-commerce

# Temporary Tables
tmp_table_size = 128M                       # Avoid excessive memory use for temp tables
max_heap_table_size = 128M                  # Ensure heap tables don’t consume too much memory

# Query Optimizations
query_cache_size = 0                       # Keep disabled for modern workloads and PHP applications
query_cache_type = 0                       # Disable query cache (not recommended for PHP apps)

# Buffer and Sort Settings
join_buffer_size = 32M                      # Safe default for small joins
sort_buffer_size = 8M                      # Small buffer for sorting
read_buffer_size = 4M                      # Efficient for reading operations
read_rnd_buffer_size = 8M                  # Efficient random read buffer

table_definition_cache = 2400
performance_schema = ON

# Logging
log_error = /var/log/mysql/mariadb-error.log  # Error log location

# Binary Logging (optional, if using replication or backups)
log_bin = /var/log/mysql/mariadb-bin
expire_logs_days = 10                      # Safe retention period for logs
max_binlog_size = 100M

# * Fine Tuning
#

key_buffer_size        = 4M
#max_allowed_packet     = 1G
#thread_stack           = 192K
#thread_cache_size      = 8
# This replaces the startup script and checks MyISAM tables if needed
# the first time they are touched
#myisam_recover_options = BACKUP
#max_connections        = 100
#table_cache            = 64

#
# * Logging and Replication
#

# Note: The configured log file or its directory need to be created
# and be writable by the mysql user, e.g.:
# $ sudo mkdir -m 2750 /var/log/mysql
# $ sudo chown mysql /var/log/mysql

# Both location gets rotated by the cronjob.
# Be aware that this log type is a performance killer.
# Recommend only changing this at runtime for short testing periods if needed!
#general_log_file       = /var/log/mysql/mysql.log
#general_log            = 1

# When running under systemd, error logging goes via stdout/stderr to journald
# and when running legacy init error logging goes to syslog due to
# /etc/mysql/conf.d/mariadb.conf.d/50-mysqld_safe.cnf
# Enable this if you want to have error logging into a separate file
#log_error = /var/log/mysql/error.log
# Enable the slow query log to see queries with especially long duration
#log_slow_query_file    = /var/log/mysql/mariadb-slow.log
#log_slow_query_time    = 10
#log_slow_verbosity     = query_plan,explain
#log-queries-not-using-indexes
#log_slow_min_examined_row_limit = 1000

# The following can be used as easy to replay backup logs or for replication.
# note: if you are setting up a replica, see README.Debian about other
#       settings you may need to change.
#server-id              = 1
#log_bin                = /var/log/mysql/mysql-bin.log
expire_logs_days        = 10
#max_binlog_size        = 100M

#
# * SSL/TLS
#

# For documentation, please read
# https://mariadb.com/kb/en/securing-connections-for-client-and-server/
#ssl-ca = /etc/mysql/cacert.pem
#ssl-cert = /etc/mysql/server-cert.pem
#ssl-key = /etc/mysql/server-key.pem
#require-secure-transport = on

#
# * Character sets
#

# MySQL/MariaDB default is Latin1, but in Debian we rather default to the full
# utf8 4-byte character set. See also client.cnf
character-set-server  = utf8mb4
collation-server      = utf8mb4_general_ci

#
# * InnoDB
#

# InnoDB is enabled by default with a 10MB datafile in /var/lib/mysql/.
# Read the manual for more InnoDB related options. There are many!
# Most important is to give InnoDB 80 % of the system RAM for buffer use:
# https://mariadb.com/kb/en/innodb-system-variables/#innodb_buffer_pool_size
#innodb_buffer_pool_size = 8G

# this is only for embedded server
[embedded]

# This group is only read by MariaDB servers, not by MySQL.
# If you use the same .cnf file for MySQL and MariaDB,
# you can put MariaDB-only options here
[mariadb]

# This group is only read by MariaDB-10.11 servers.
# If you use the same .cnf file for MariaDB of different versions,
# use this group for options that older servers don't understand
[mariadb-10.11]
EOF
mkdir -p /var/log/mysql
mkdir -p /etc/systemd/system/docker.service.d
systemctl enable mariadb
systemctl restart mariadb
cat > /etc/systemd/system/docker.service.d/override.conf <<EOF
[Service]
# Limit to 2 CPU cores
CPUQuota=1200%
# Limit to 4GB of RAM
MemoryLimit=18G
EOF
systemctl daemon-reload
systemctl restart docker
mysql -u root <<< "CREATE USER 'gitlab-runner'@'localhost' IDENTIFIED VIA unix_socket; GRANT ALL PRIVILEGES ON *.* TO 'gitlab-runner'@'localhost' WITH GRANT OPTION; FLUSH PRIVILEGES;"


cat > /root/swarm/phpmyadmin/php.ini <<EOF
allow_url_fopen = Off
post_max_size = 800M
upload_max_filesize = 800M
max_execution_time = 5000
max_input_time = 5000
memory_limit = 1024M
max_input_vars = 100000
EOF

cat > /root/swarm/phpmyadmin/docker-compose.yaml <<EOF
version: '3.7'

networks:
  traefik-net:
    name: traefik-net
    external: true
    driver: overlay
    attachable: true

services:
  phpmyadmin:
    image: phpmyadmin/phpmyadmin:5.2.1
    deploy:
      mode: global
      update_config:
        order: start-first
      labels:
        # Enable Traefik for this service
        - traefik.enable=true

        # Define the router
        - traefik.http.routers.phpmyadmin.rule=Host(\`phpmyadmin-webh-stage01.test-site.pl\`)
        # Service to use
        - traefik.http.services.phpmyadmin.loadbalancer.server.port=80
        - traefik.http.routers.phpmyadmin.tls.certresolver=myresolver
    networks:
    - traefik-net
    configs:
    - source: php_config_v3
      target: /usr/local/etc/php/conf.d/phpmyadmin-misc.ini
    environment:
      PMA_HOST: "172.17.0.1"
      PMA_PORT: "3306"
configs:
  php_config_v3:
    file: ./php.ini
EOF

cat > /root/swarm/phpmyadmin/script.sh <<EOF
#!/bin/bash -eux
docker stack deploy phpmyadmin -c docker-compose.yaml
EOF
chmod +x /root/swarm/phpmyadmin/script.sh
(
  cd /root/swarm/phpmyadmin
  ./script.sh
)
