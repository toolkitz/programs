
[[_TOC_]]

# What are programs ?

Programs are utility tools that are written to speed up php applications development process

# How to install programs ?

* First add them to your path by modifying your `~/.zshrc` / `~/.bashrc` file with the following lines:

```bash
if [ -d "$HOME/repos/agmar/toolkitz/programs" ]; then
    export PATH="$HOME/repos/agmar/toolkitz/programs/bin:$PATH"
    source "$HOME/repos/agmar/toolkitz/programs/.rc"
fi
```

or change `"$HOME/repos/agmar/toolkitz/programs"` to other path where you have this repo (this one is defeault for `tsrc` root located in `$HOME/repos/agmar`)

* Finally add these lines to your `~/.ssh/config` file replacing username with the user created by the admin for you.

```ssh-config
Host stage
   HostName 185.243.55.39
   User <YOUR USERNAME>

Host prod
   HostName 91.228.196.177
   User <YOUR USERNAME>
```

# How to use programs ?

With previous instruction you've added programs to your $PATH. That means that within next opened shell session after editing the file you will have access to them by name. Each of them has got `--help` flag which will in details explain what each program does. e.g.:

```shell
root@pc: project-copy --help
```

If you see response like:

```shell
command not found: project-copy
```

Make sure you've started new shell session and saved modified `.bashrc` / `.zshrc` file. You can check your current path by running:

```shell
root@pc: echo $PATH | sed 's|:|\n|g'
```

You should see in there reference to `bin` directory in this repository. If you don't see but want to make sure you can try to grep it. Below command assumes that your shell session is in root of this git repository.

```shell
root@pc: echo $PWD
> /Users/mikolajmlodzikowski/repos/agmar/programs
root@pc: echo $PATH | sed 's|:|\n|g' | grep $PWD
> /Users/mikolajmlodzikowski/repos/agmar/programs/bin
```

# Auto git pull and docker pull (optional, but recommended)

If you want to automate `git pull` and `docker-compose pull` commands (because sometimes you forget to do that) you can source this file too:

```shell
if [ -d "$HOME/repos/agmar/programs" ]; then
    source "$HOME/repos/agmar/programs/chpwd"
fi
```

This will trigger shell hook whenever you enter the directory. This hook will check if there is `.git` directory and if it is not locked trigger `git pull --rebase`. If there is `docker-compose.yml` file it will trigger `dcc pull app` to get newest image to work with.

# Currently maintained programs

* `project-copy` -> used to copy entire project into repo
* `ssh-d` -> used to execute command in remote docker container
* `ssh-d-cp` -> used to copy files from remote docker container

# Additional config

```shell
glab alias set get-variable 'glab api /projects/:id/variables/$1 | jq .value' --shell
```
