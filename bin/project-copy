#!/bin/bash -e

usage() {
    echo "This command line tool is used to copy a project state (backend + dump directories) inside the repository from a server. It works only when directories are clean (no changes and no untracked files). Links in database are replaced and commit is created (but not pushed). You need to be located in the root directory of the repository, otherwise you will need to move and modify downloaded files manually."
    echo "--server | -s is argument that takes server name as argument, so for example --server stage (if you've configured ssh!), otherwise you need to provie IP or DNS entry. Default: prod"
    echo "--project | -p is argument that takes project name as argument, so for example --project topfryz-pl. By default project name from gitlab will be taken if you are located inside the repository which need to be copied. IF PROVIDED MANUALLY: it needs to be full project name without groups."
    echo "--exclude-dump allows you to skip copying dump from remote."
    echo "--exclude-backend allows you to skip copying backend from remote."
    echo "--skip-commit allows you to skip making commit."
    echo "--trace allow to show line by line what happens within the script."
    echo "--allow-db-drop allows script to drop existing local database, otherwise you will be prompted about it."
}

while [[ $# -gt 0 ]]; do
    num="$#"
    key="$1"
    case $key in
    -s | --server)
        server="$2"
        shift
        shift
        ;;
    -p | --project)
        project="$2"
        shift
        shift
        ;;
    --exclude-dump | --skip-dump | --skip-db | --exclude-db)
        exclude_dump="true"
        shift
        ;;
    --exclude-backend | --skip-backend)
        exclude_backend="true"
        shift
        ;;
    --exclude-commit | --skip-commit)
        skip_commit="true"
        shift
        ;;
    --allow-db-drop | --db-drop)
        db_drop="true"
        shift
        ;;
    -h | --help)
        usage
        exit 0
        ;;
    --trace)
        set -x
        shift
        ;;
    esac
    if [[ $num -eq $# ]]; then
        script_name="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
        echo "You provided wrong arguments (cannot parse: $1), check how to work with this tool by running '$script_name --help'"
        exit 1
    fi
done

check_glab

if ! command -v jq &> /dev/null; then
    echo 'You need to install jq command line tool from https://stedolan.github.io/jq/download/ before continue.'
    exit 1
fi

if [ -z "$project" ]; then
    if glab api projects/:fullpath | jq -r '.name' &> /dev/null; then
        project="$(glab api projects/:fullpath | jq -r '.name')"
    else
        echo "If you use project-copy without --project flag you need to be located at the root of the repository where you run copy"
    fi
fi

if [ -z "$server" ]; then
    include=$(glab api "/projects/:id/repository/files/.gitlab-ci.yml?ref=master" | jq -r '.content' | base64 -d | grep webh)
    if echo $include | grep '01'; then
        server="prod-backend01"
    elif echo $include | grep '02'; then
        server="prod-backend02"
    elif echo $include | grep '03'; then
        server="prod-backend03"
    else
        echo "couldn't find server automatically in .gitlab-ci.yml, please provide server name manually"
    fi
    echo "using server: $server"
fi

check_git_clean() {
    if ! git diff --exit-code $1 &> /dev/null; then
        git status
        echo ">> ERROR: Automated commit will be created, but you have uncommited changed in '$1'"
        echo ">> fix 1 discard changes: git restore $1"
        echo ">> fix 2 commit changes: git add $1 && git commit -m 'your message'"
        exit 1
    fi
    if [[ ! -z "$(git ls-files --others --exclude-standard $1)" ]]; then
        git status
        echo ">> ERROR: Automated commit will be create, but you have untracked files in '$1'"
        echo ">> fix 1 remove files: git ls-files --others --exclude-standard $1 | xargs rm -rf"
        echo ">> fix 2 add files: git add $1 && git commit -m 'your message'"
        exit 1
    fi
}

make_commit() {
    if [ -z "$skip_commit" ]; then
        if ! git diff --exit-code $1 &> /dev/null || [[ ! -z "$(git ls-files --others --exclude-standard $1)" ]]; then
            git add $1
            git commit -m "[project-copy] Add $1 content from ${server}"
        else
            echo ">> Nothing to commit in $1!"
        fi
    fi
}

remove_previous() {
    if [ -f "$1.tgz" ]; then
        echo ">> Found $1 file from previous copy, removing it"
        rm $1.tgz
    fi
}

# check if we work on clean repository before update
if [ -z "$exclude_backend" ] && [ -d "backend" ]; then
    check_git_clean backend
fi

if [ -z "$exclude_dump" ] && [ -d "dump" ]; then
    check_git_clean dump
fi

if echo $server | grep -q shared; then
    if [ -z "$exclude_backend" ]; then
        domain=$(glab api /projects/:fullpath/variables/HOST_PROD | jq -r '.value')
        copy-backend-shared $server $domain
        make_commit backend
    fi
else
    if [[ -z "$exclude_backend" ]]; then
        echo ">> Copying backend"
        remove_previous backend
        ssh-d --server "${server}" --project "${project}" --command "pack-backend" $flags
        # skip archive because archive is created inside container in the previous step
        ssh-d-cp --skip-archive --server "${server}" --project "${project}" --source "/tmp/backend.tgz" --destination "./backend.tgz" $flags
        mkdir -p tmp
        mv backend.tgz tmp/
        (
            cd tmp
            tar zxf backend.tgz
        )
        if [ -d "backend" ]; then
            echo ">> Backend directory detected, you are inside the repo!"
            echo ">> Removing current backend directory"
            s3_dirs=$(cat .env | grep -i s3_sync_dirs | cut -d '=' -f2)
            for dir in $s3_dirs; do
                if [ -d "backend/$dir" ]; then
                    echo ">> Moving s3 directory located at: 'backend/$dir' to temporary directory 's3_content/$dir'"
                    if [ -d "backend/$dir" ]; then
                        mkdir -p "$(dirname "s3_content/$dir")"
                        mv "backend/$dir" "s3_content/$dir"
                    fi
                fi
            done
            rm -rf backend
            echo ">> Updating backend directory with server state"
            mv "$(find tmp -type d -name backend | head -n 1)" "backend"
            echo ">> Removing leftovers from unpacking"
            rm -rf tmp
            for dir in $s3_dirs; do
                if [ -d "s3_content/$dir" ]; then
                    echo ">> Moving s3 directory located at: 's3_content/$dir' back to 'backend/$dir'"
                    if [ -d "s3_content/$dir" ]; then
                        if [ -d "backend/$dir" ]; then
                            rm -rf "backend/$dir"
                        fi
                        mkdir -p "$(dirname "backend/$dir")"
                        mv "s3_content/$dir" "backend/$dir"
                    fi
                fi
            done
            while read -r config_file; do
                echo ">> Restoring config file: $config_file to local settings"
                git restore $config_file || echo "$config_file is already in default version"
            done< <(rg -l TERRAFORM)
            rm -rf s3_content/
            make_commit backend
        fi
        echo ">> Removing backend's archive"
    else
        echo ">> Skipping backend copy!"
    fi
fi


if [ -z "$exclude_dump" ]; then
    if [ $(uname) = "Linux" ]; then
        export DCC_HOST="host.docker.internal"
    else
        export DCC_HOST="docker.for.mac.host.internal"
    fi
    if echo $server | grep -q shared; then
        user=$(glab api /projects/:fullpath/variables/MYSQL_PROD_USERNAME | jq -r '.value')
        db=$(glab api /projects/:fullpath/variables/MYSQL_PROD_DATABASE | jq -r '.value')
        password=$(glab api /projects/:fullpath/variables/MYSQL_PROD_PASS | jq -r '.value')
        ssh -t $server "mysqldump --user=$user --password=$password $db | tail -n +2 > dump.sql"
        scp $server:dump.sql dump.sql
        mv dump.sql dump/app.sql
    else
        echo ">> Making dump by dumper"
        ssh-d --server "${server}" --project "${project}" --command "dumper --dump all" $flags
        echo ">> Copying dump"
        remove_previous dump
        ssh-d-cp --server "${server}" --project "${project}" --source "/dump" --destination "./dump.tgz" $flags
        tar zxf dump.tgz
        echo ">> Dump directory detected, you are inside the repo!"
        echo ">> Replacing app dumps with state from server"
        rm -rf "dump/app"
        rm -rf "dump/init"
        mv $(find tmp/ -iname 'init.sql') dump/init.sql
        mv $(find tmp/ -iname 'app.sql') dump/app.sql
        echo ">> Removing leftovers from unpacking"
        rm -rf tmp
    fi
    case $server in
        *prod*|*shared*)
            var="HOST_PROD"
            ;;
        *stage*)
            var="HOST_STAGE"
            ;;
        *)
            echo ">> ERROR: Unknown server to read host var"
            exit 1
            ;;
    esac
    echo "using var for finding host: $var"
    echo glab api "/projects/:fullpath/variables/$var"
    host_to_replace=$(glab api "/projects/:fullpath/variables/$var" | jq -r '.value')
    if [[ "$host_to_replace" = "null" ]] || [[ -z "$host_to_replace" ]]; then
        echo ">> ERROR: Host to replace is empty or null, please check if you have proper variable set in gitlab"
        exit 1
    fi
    echo ">> Replacing $host_to_replace with localhost:8088"

    if [ -d "dump" ]; then
        if cat .env | grep -i gitlab_full_path | grep -iq presta; then
            echo ">> Running sed on dump files"
            if [[ "$server" = "*shared*" ]]; then
                echo ">> Warning!! Shared server detected - using entire db as app.sql, in case of failures watch out what's commited"
                files="dump/app.sql"
            else
                files="dump/init.sql dump/app.sql"
            fi
            for file in $files; do
                sed -i "s|https://${host_to_replace}|http://localhost:8088|g" $file
                sed -i "s|http://${host_to_replace}|http://localhost:8088|g" $file
                sed -i "s|https:\\/\\/${host_to_replace}|http:\\/\\/localhost:8088|g" $file
                sed -i "s|http:\\/\\/${host_to_replace}|http:\\/\\/localhost:8088|g" $file
            done
        fi
        echo ">> Starting app"
        volume_name="${project}_db_data"
        if ! docker volume list | grep -q $volume_name; then
            docker volume create --name=$volume_name
        fi
        while docker ps | grep -q 8088; do
            sleep 1 && echo "other docker is running, waiting"
            docker ps | grep 8088
        done
        docker compose up -d
        echo ">> Waiting for mysql to be ready"
        until docker compose exec app connect_mysql --execute='SELECT 1'; do
            sleep 1
        done
        echo ">> Cleaning up database"
        if [ -z "$db_drop" ]; then
            while true; do
                read -p ">> You haven't added '--allow-db-drop' flag, are you ok with removing local database content? WARNING! To make sure database is consistent with the remote state this has to be done anyway. Please respond 'yes' if you wish to continue, 'no' if you want to exit program and leave current work as it is. Please note that you can dump your current work in another terminal windows and proceed with the script after that: >> " yn
                case $yn in
                    [Yy]* ) break;;
                    [Nn]* ) exit 1;;
                    * ) echo "Please answer yes or no.";;
                esac
            done
        fi
        docker compose exec app dumper --reset
        echo ">> Loading dumps"
        if echo $server | grep -q shared; then
            docker compose exec app dumper --load dump/app.sql
        else
            docker compose exec app dumper --load dump/app.sql
            docker compose exec app dumper --load dump/init.sql
        fi
        if cat .env | grep -i gitlab_full_path | grep -iq wordpress; then
            echo ">> Init wordpress admin"
            docker compose exec app dumper --init-wordpress-admin
            echo ">> Replacing serials in the database"
            docker compose exec app dumper --fix-serial localhost:8088 $host_to_replace http
        elif cat .env | grep -i gitlab_full_path | grep -iq presta; then
            echo ">> Init presta shop url and ssl settings"
            docker compose exec app dumper --init-presta-host
            echo ">> Init presta admin password"
            docker compose exec app dumper --init-presta-admin
        fi
        echo ">> Dumping adjusted database"
        docker compose exec app dumper --dump all
        docker compose down
        make_commit dump
    fi
    if ! echo $server | grep -q shared; then
        if [ -f "dump.tgz" ]; then
            echo ">> Removing dump's archive"
            rm dump.tgz
        fi
        echo ">> Cleanup dump directory in container"
        ssh-d --server "${server}" --project "${project}" --command "rm -rf /dump" $flags
    else
        echo ">> Removing dump's archive from shared"
        ssh -t $server "rm dump.sql"
    fi
else
    echo ">> Skipping dump copy!"
fi

unset DCC_HOST